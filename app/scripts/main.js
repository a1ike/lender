$(document).ready(function () {

  $('.phone').inputmask('+7(999)999-99-99');

  $('.l-header__toggle').on('click', function (e) {
    e.preventDefault();
    $('.l-header__info').slideToggle('fast');
    $('.l-header__nav').slideToggle('fast');
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.l-modal').slideToggle('fast');
  });

  $('.l-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.l-modal').slideToggle('fast');
  });


  $('.l-home-top').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        dots: false
      }
    }]
  });

  $('.l-home-companies__cards').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }]
  });

  $('.l-home-help__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    prevArrow: $('.l-home-help__arrow_prev'),
    nextArrow: $('.l-home-help__arrow_next'),
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false
      }
    }]
  });

  $('.l-best__cards').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }]
  });

  $('.l-tabs__link').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.l-tabs__link').removeClass('l-tabs__link_current');
    $('.l-tabs__content').removeClass('l-tabs__content_current');

    $(this).addClass('l-tabs__link_current');
    $('#' + tab_id).addClass('l-tabs__content_current');
  });

});
